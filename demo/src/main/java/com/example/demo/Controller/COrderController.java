package com.example.demo.Controller;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import com.example.demo.Model.COrder;
import com.example.demo.Model.CUser;
import com.example.demo.Respository.iOrderRespository;
import com.example.demo.Respository.iUserRepository;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
// thêm đường dẫn vd : v1/order/all
@RequestMapping(path = "/v1/order")
public class COrderController {
    @Autowired
    iOrderRespository iOrderRepository;

    @Autowired
    iUserRepository iUserRepository;

    @GetMapping(path = "/all")
    public ResponseEntity<Object> getAllorders(){
        List<COrder> orderList = new ArrayList<COrder>();
        try {
            // orderElement là tham số đặt tên tự do
            iOrderRepository.findAll().forEach(orderElement -> {
                orderList.add (orderElement);
            });
            return new ResponseEntity<Object>(orderList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/search/userId={id}")
    public ResponseEntity<Set<COrder>> getOrdersByUserId(@PathVariable long id){
        try {
            // ko xài findById của thư viện dc vì ở đây là 1 liên kết nên phải tạo 1 hàm findById bên iUserRepository
            // và ko xài Optional <CUser> nếu ko liên kết có thể xài thư viên findById và optinal <Order>
            
            CUser user = iUserRepository.findById(id);
            if(user != null){
                return new ResponseEntity<Set<COrder>>(user.getOrders(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(path = "/detail/{id}")
    public ResponseEntity<Object> getOrderById(@PathVariable (name = "id") Long id) {
        Optional<COrder> order = iOrderRepository.findById(id) ;
        if(order.isPresent()) {
            return new ResponseEntity<Object>(order, HttpStatus.OK);
        } else {
            return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(path = "/create")
    public ResponseEntity<Object> create0rder(@RequestBody COrder newOrder) {
        COrder order = new COrder();
        try {
            order.setCreated(new Date());
            order.setUpdated(null);
            order.setOrderCode(newOrder.getOrderCode());
            order.setVoucherCode(newOrder.getVoucherCode());
            order.setPizzaSize(newOrder.getPizzaSize());
            order.setPizzaType(newOrder.getPizzaType());
            order.setPrice(newOrder.getPrice());
            order.setPaid(newOrder.getPaid());
            iOrderRepository.save(order);
            return new ResponseEntity<Object>(order, HttpStatus.OK);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                .body("Can not execute operation about this Entity " +e.getCause().getMessage());
        }
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateOrder(@PathVariable (name = "id") Long id, @RequestBody COrder orderUpdate) {
        Optional<COrder> orderData = iOrderRepository.findById(id);
        if (orderData.isPresent()) {
            COrder order = orderData.get();
            order.setUpdated(new Date()) ;
            order.setPizzaSize(orderUpdate.getPizzaSize());
            order.setPizzaType(orderUpdate.getPizzaType());
            order.setPrice(orderUpdate.getPrice());
            order.setPaid(orderUpdate.getPaid());
            order.setVoucherCode(orderUpdate.getVoucherCode());
            try {
                return ResponseEntity.ok(iOrderRepository.save(order));
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                .body("Can not execute operation of this Entity" + e.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<Object>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> deleteOrderById(@PathVariable Long id) {
        try {
            iOrderRepository.deleteById(id) ;
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
            .body("Can not execute operation of this Entity"+ e.getCause().getCause().getMessage());
        }
    }
}

