package com.example.demo.Controller;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import com.example.demo.Model.CUser;
import com.example.demo.Respository.iUserRepository;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping(path = "/v1/user")
public class CUserController {
    @Autowired
    iUserRepository iUserRepository;

    @GetMapping(path = "/all")
    public ResponseEntity<Object> getAllUsers() {
        List<CUser> userList = new ArrayList<CUser> ();
        // userElement là tham số đặt tên tự do
        iUserRepository.findAll().forEach(userElement ->{
            userList.add(userElement);
        });
        if (!userList.isEmpty())
            return new ResponseEntity<Object>(userList, HttpStatus.OK);
        else {
            return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
        }
    }


    @GetMapping("/detail")
    public ResponseEntity<Object> getuserById(@RequestParam(name = "id", required = true) Long id) {
        Optional<CUser> userFounded = iUserRepository.findById(id);
        if (userFounded.isPresent())
            return new ResponseEntity<Object>(userFounded, HttpStatus.OK);
        else {
            return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/create")
    public ResponseEntity<Object> createUser(@RequestBody CUser userFromClient) {
        try {
            CUser user = new CUser(userFromClient.getFullname(), userFromClient.getEmail(), userFromClient.getPhone(), userFromClient.getAddress());
            Date now = new Date();
            user.setCreated(now);
            user.setUpdated(null);
            iUserRepository.save(user);
            return new ResponseEntity<Object>(user, HttpStatus.OK);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity().body("Failed to Create specified user: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updatelUser(@PathVariable (name ="id") Long id, @RequestBody CUser userUpdate) {
        Optional<CUser> userData = iUserRepository.findById(id);
        if (userData.isPresent()){
            CUser user = userData.get();
            user.setFullname(userUpdate.getFullname());
            user.setEmail(userUpdate.getEmail());
            user.setPhone(userUpdate.getPhone()) ;
            user.setAddress(userUpdate.getAddress());
            user.setUpdated(new Date());
            try {
                return ResponseEntity.ok(iUserRepository.save(user));
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                    .body("Can not execute operation of this Entity"+ e.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<Object> (null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }         
    
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> deletelUser(@PathVariable("id") Long id){
        Optional<CUser> userData = iUserRepository.findById(id) ;
        if (userData.isPresent()) {
            try {
                iUserRepository.deleteById(id);
                return new ResponseEntity<Object>(HttpStatus.NO_CONTENT) ;
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                        .body("Can not execute operation of this Entity" + e.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<Object>("User not found! ", HttpStatus.NOT_FOUND);
        }
    } 
}


