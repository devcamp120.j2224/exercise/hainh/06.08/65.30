package com.example.demo.Respository;
import org.springframework.data.jpa.repository.JpaRepository;
import com.example.demo.Model.COrder;

public interface iOrderRespository extends JpaRepository <COrder , Long>{
    
}
